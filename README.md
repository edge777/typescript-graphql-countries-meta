# Typescript-GraphQl- Countries meta information

## Features

- ExpressJS
- Apollo GraphQL
- TypeScript
- Code formating & linting
  - [Prettier](https://prettier.io/) `npm run format`
  - [pre-commit](https://pre-commit.com/) - `pre-commit install`


## Getting started
- npm install
- npm run start

