export type CountryBasicResponse = {
  name: string,
  native: string,
  currency: string,
  capital: string,
  phone: string,
  languages: LanguageBasicResponse,
  continent: ContinentBasicResponse
}

export type LanguageBasicResponse = {
  name: string,
  code: string
}

export type ContinentBasicResponse = {
  name: string,
  code: string
}