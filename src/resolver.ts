import { GraphQLResolveInfo } from 'graphql';
import { IResolvers } from 'graphql-tools';
const fs = require('fs'); 
const parse = require('csv-parse');
const path = require('path');
import { CountryBasicResponse, LanguageBasicResponse, ContinentBasicResponse } from "./types";

const resolver: IResolvers = {
  Query: {
    async country(_: void, args:{ code: string }, info: GraphQLResolveInfo) {
      return await getCountriesDate(args.code);
    },
  },
};

async function getCountriesDate(code: string): Promise<CountryBasicResponse | null> {
  return new Promise((resolve, resject) => {
    let dataFound = false;
    fs.createReadStream(path.join(__dirname, "/assets/countries.csv"))
          .pipe(parse({delimiter: ','}))
          .on('data', async function(csvrow: any) {
              if(csvrow.includes(code)) {
                dataFound = true;
                const lanCode = csvrow[7]
                const languages = await getLanguageDate(lanCode);
                const continentName = csvrow[4]
                const continent = await getContinentDate(continentName);
                resolve({
                  name: csvrow[1],
                  native: csvrow[2],
                  currency: csvrow[6],
                  capital: csvrow[5],
                  phone: csvrow[3],
                  languages,
                  continent
                })               
              }
          })
          .on('end', () => {
            if(!dataFound) {
              resolve(
                null
              )
            }

          })
  })
}

async function getLanguageDate(code: string): Promise<LanguageBasicResponse> {
  return new Promise((resolve, reject) => {
    fs.createReadStream(path.join(__dirname, "/assets/languages.csv"))
          .pipe(parse({delimiter: ','}))
          .on('data', function(csvrow: any) {
              if(csvrow.includes(code)) {
                resolve({
                  name: csvrow[1],
                  code: csvrow[0]
                })       
              } 
          })
  })
}

async function getContinentDate(name: string): Promise<ContinentBasicResponse> {
  return new Promise((resolve, reject) => {
    fs.createReadStream(path.join(__dirname, "/assets/continents.csv"))
          .pipe(parse({delimiter: ','}))
          .on('data', function(csvrow: any) {
              if(csvrow.includes(name)) {
                resolve({
                  name: csvrow[1],
                  code: csvrow[0]
                })     
              }
          })
  })
}

export default resolver;
